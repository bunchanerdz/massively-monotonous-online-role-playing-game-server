let data = require('../data/data.js');
let item = require('../util/item.js');
let random = require('../util/random.js');

let Item = item.Item;

module.exports.generateLootItem = (tier) => {
	if (tier === undefined) {
		let df = data.dataFiles;

		let i = new Item();

		i.lootType = random.fromArray(df['Types']);

		if (i.lootType.HasQuality === 'True') {
			i.quality = random.fromArray(df['Qualities']);
		}

		i.type = random.fromArray(df[i.lootType.Name]);

		let materials = df[i.lootType.Name + 'Materials'];

		if (i.lootType.CanBeElemental === 'True') {
			i.element = random.fromWeightedArray(df['Elements']);
		}

		if (materials !== undefined) {
			i.material = random.fromArray(materials);
		}

		return i;
	}
}
