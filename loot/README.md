# Loot API Server

The loot API server is a microservice intended to provide loot generation in an isolated subservice.

## Example to build the docker container

./build.sh

## Example to run the docker container

docker run --restart unless-stopped -p 3000:3000 -d loot.api.server
