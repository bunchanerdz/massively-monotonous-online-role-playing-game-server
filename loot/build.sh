#!/bin/bash

TMP=tmp

git clone .. $TMP

cp -r ../node_modules $TMP/
cp -r ../data $TMP/
mv $TMP/config/config.example.js $TMP/config/config.js

docker build -t loot.api.server .

rm -rf $TMP
