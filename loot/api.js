let loot = require('./loot.js');

class lootApiServer {
	constructor(options) {
		this.appServer = options.appServer;

		this.appServer.get('/gen', (req, res) => {
			res.send(loot.generateLootItem());
		});

		this.appServer.get('/gen/clean', (req, res) => {
			let i = loot.generateLootItem().toString();

			res.send(i.toString());
		});
	}
}

module.exports = lootApiServer;
