module.exports = {
	port: 3000,
	aud: 'game-server-verify-account',
	iss: 'https://accounts.example.com',
	jwt_secret: 'CHANGE_ME',
};
