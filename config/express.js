var passport = require('passport');

passport.serializeUser((user, done) => {
	LogService.log("Serializing user");
	done(null, user.uid);
});

passport.deserializeUser((uid, done) => {
	LogService.log("Deserializing user");
	User.findOne({uid: uid}, (err, user) => {
		done(err, user);
	});
});

module.exports.http = {
	customMiddleware: (app) => {
	},
};
