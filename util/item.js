class Item {
	constructor(options) {
	}

	getAL() {
		console.log(this.type);
		return this.type['Base AL'];
	}

	getBurden() {
		return 0;
	}

	getDamageLower() {
		return 0;
	}

	getDamageUpper() {
		return 0;
	}

	getDamageModifier() {
		return 0;
	}

	getValue() {
		return 0;
	}

	toString() {
		let str = '';

		if (this.quality) {
			str += this.quality.Name + ' ';
		}

		if (this.element && this.element.Name !== "N/A") {
			str += this.element.Name + ' ';
		}

		if (this.material) {
			str += this.material.Name + ' ';
		}

		if (this.type) {
			str += this.type.Name + ' ';
		}

		if (str.endsWith(' ')) {
			str = str.substr(0, str.length - 1);
		}

		console.log(str);
		return str;
	}
}

module.exports.Item = Item;
