function nextInt(low, high) {
	return Math.floor(Math.random() * (high - low + 1) + low);
};

function fromArray(ray) {
	return ray[nextInt(0, ray.length - 1)];
};

function fromWeightedArray(ray) {
	let weightSum = ray.reduce((sum, item) => {
		return sum + parseInt(item.Weight);
	}, 0);

	let i = nextInt(0, weightSum - 1);

	return ray.find((item) => {
		i -= item.Weight;

		return i < 0;
	});
}

module.exports.fromArray = fromArray;
module.exports.nextInt = nextInt;
module.exports.fromWeightedArray = fromWeightedArray;
