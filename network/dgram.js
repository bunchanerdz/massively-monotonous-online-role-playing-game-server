const dgram = require('dgram');
const server = dgram.createSocket('udp4');
const Buffer = require('buffer').Buffer;

server.on('error', (err) => {
	console.log(err);
});

server.on('message', (msg, rinfo) => {
	console.log(msg);

	let id = msg.readInt32BE(0);
	let token = msg.readInt32BE(4);

	console.log(id);
	console.log(token);

	//console.log(rinfo);
});

server.on('listening', () => {
	console.log("Datagram/UDP listening...");
});

server.bind(51496);
