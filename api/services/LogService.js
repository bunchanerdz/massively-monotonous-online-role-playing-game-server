Object.defineProperty(global, '__info', {
	/*
	get: () => {
		let index = 1;

		var orig = Error.prepareStackTrace;
		Error.prepareStackTrace = (_, stack) => {
			return stack;
		};
		var err = new Error;

		Error.captureStackTrace(err, arguments.callee);

		var stack = err.stack;

		Error.prepareStackTrace = orig;

		if (stack[index]) {
			return stack[index].getFileName() + "." + stack[index].getFunction() + " (" + stack[index].getLineNumber() + "): ";
		} else {
			console.log(stack);
			return 'null.null (0): ';
		}
	}
	*/

	get: () => {
		let st = require('stack-trace');
		let trace = st.get();
		let index = 3;

		let re1 = new RegExp('^.*/');
		let re2 = new RegExp('\.[^\.]*$');

		return trace[index].getFileName().replace(re1, '').replace(re2, '') + '.' + trace[index].getFunctionName() + ' (' + trace[index].getLineNumber() + '): ';
	}
});

module.exports = {
	log: (data) => {
		process.stdout.write(__info);

		console.log(data);
	}
};
