// Load data
let parse = require('csv-parse/lib/sync');
let fs = require('fs');
let glob = require('glob');
let path = require('path');

let datafiles = 0;
let loaded = 0;

const EventEmitter = require('events');

class DataEmitter extends EventEmitter {
}

const emitter = new DataEmitter();

module.exports.dataFiles = [];
module.exports.emitter = emitter;

glob('./data/**/*.csv', (err, files) => {
	files.forEach((filename) => {
		let shortName = path.basename(filename, '.csv');
		fs.readFile(filename, {encoding: 'utf16le'}, (err, data) => {
			if (err) {
				throw err;
			}

			emitter.emit('loading', shortName, data.length);

			module.exports.dataFiles[shortName] = parse(data, {columns: true});

			++loaded;

			emitter.emit('loaded', shortName, data.length, module.exports.dataFiles[shortName].length);

			if (files.length == loaded) {
				emitter.emit('completed');
			}
		});
	});
});


module.exports.emitter.on('loaded', (filename, len, items) => {
	//LogService.log('Loaded: ' + filename + ' of length ' + len + ' with a total of ' + items + ' items.');
});

module.exports.emitter.on('completed', () => {
	LogService.log('Data loaded.');
});

