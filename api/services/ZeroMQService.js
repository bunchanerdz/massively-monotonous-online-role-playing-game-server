let zeromq = require('zeromq');
let pub = zeromq.socket('pub');

pub.bindSync('tcp://0.0.0.0:51500');

setInterval(() => {
	Character.find({}).exec((err, characters) => {
		if (err) LogService.log(err);
		else pub.send(['landblock1', JSON.stringify({characters: characters})]);
	});
}, 500);
