/**
 * CharacterController
 *
 * @description :: Server-side logic for managing characters
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	list: (req, res) => {
		Character.find({
		}).exec((err, characters) => {
			if (err) {
				return res.json({});
			}

			res.json({characters: characters});
		});
	},
	create: (req, res) => {
		Jwt.getJwt(req, (err, jwt) => {
			if (err) {
				LogService.log(err);
			}

			Character.create({
				name: req.params.all().name,
				uid: jwt.body.sub,
			}).exec((err, character) => {
				if (err) {
					LogService.log(err);

					return res.view('500');
				} else {
					LogService.log("Created character ", character);
					Character.find({uid: req.uid}).exec((err, results) => {
						if (err) LogService.log(err);
						LogService.log(results);
					});

					return res.json(character);
				}
			});
		});
	},
	updateLoc: (req, res) => {
		Jwt.getJwt(req, (err, jwt) => {
			if (err) {
				LogService.log(err);

				return res.status(403).json({"message": "Invalid credentials"});
			}

			LogService.log("TODO: Validate that the new coordinates are reasonable (i.e. within reason for the time since last check in -- not to exceed some threshold, maybe 200ms)");

			Character.findOne({
				name: req.params.all().name,
				uid: jwt.body.sub,
			}).exec((err, character) => {
				if (err || !character) res.status(500).json({"message": "Unable to update character"});
				else {
					Character.update({
						name: character.name,
						uid: jwt.body.sub,
					},
					{
						locX: req.params.all().locX,
						locY: req.params.all().locY,
						locZ: req.params.all().locZ,
						rotW: req.params.all().rotW,
						rotX: req.params.all().rotX,
						rotY: req.params.all().rotY,
						rotZ: req.params.all().rotZ,
					}).exec((err, updates) => {
						res.json({'new-character': updates[0], 'old-character': character});
					});
				}
			});
		});
	},
	destroy: (req, res) => {
		Jwt.getJwt(req, (err, jwt) => {
			if (err) {
				LogService.log(err);

				return res.status(403).json({"message": "Invalid credentials"});
			}

			Character.destroy({
				name: req.params.all().name,
				uid: jwt.body.sub,
			}).exec((err) => {
				if (err) res.status(500).json({"message": "Unable to delete character"});
				else res.json({"message": "Deleted character named " + req.params.all().name + "."});
			});
		});
	},
};

