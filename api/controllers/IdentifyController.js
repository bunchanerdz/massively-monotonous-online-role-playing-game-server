/**
 * IdentifyController
 *
 * @description :: Server-side logic for managing identifies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let data = require('../../data/data.js');
let loot = require('../../loot/loot.js');

module.exports = {
	generate: (req, res) => {
		let item = loot.generateLootItem();

		console.log(item);

		let view = item.lootType.Name.toLowerCase();

		return res.view('identify/' + view, {
			locals: {
				item: item
			}
		});
	}
};

