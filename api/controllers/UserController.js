/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');
var uuid = require('uuid/v4');

module.exports = {
	jwtlist: (req, res) => {
		// TODO: Only show JWTs for active user unless admin
		Jwt.find({}).exec((err, jwts) => {
			res.json(jwts);
		});
	},
	jwt: (req, res) => {
		if (req.method === "GET") {
			res.view('user/signin', {action: '/user/jwt'});
			return;
		}

		let params = req.allParams();
		let userParams = {
			email: params.email,
			password: params.password,
			provider: 'local'
		};

		LogService.log("JWT request made");

		user = User.authenticate(userParams, (user) => {
			if (user.authenticated) {
				req.login(user, (err) => {
					if (err) {
						LogService.log(err);

						res.redirect('/user/jwt');
					} else {
						res.json({jwt:{'value': Jwt.getGameClientLogin(user)}});
					}
				});
			} else {
				res.redirect('/user/jwt');
			}
		});
	},
	list: (req, res) => {
		User.find({}).exec((err, users) => {
			LogService.log(users);
			LogService.log(users[0]);
			LogService.log(users[0].toJSON());
			res.json(users);
		});
	},
	login: (req, res) => {
		if (req.method === "GET") {
			res.redirect('/user/signin');
			return;
		}

		let params = req.allParams();
		let userParams = {
			email: params.email,
			password: params.password,
			provider: 'local'
		};

		user = User.authenticate(userParams, (user) => {
			if (user.authenticated) {
				req.login(user, (err) => {
					LogService.log(user);

					if (err) {
						return res.send(err);
					} else {
						return res.send({
							message: '',
							user: user
						});
					}
				});
			};
		});
	},
	logout: (req, res) => {
		req.logout();

		res.redirect('/');
	},
	signin: (req, res) => {
		res.view('user/signin', {action: '/user/login'});
	},
	register: (req, res) => {
		if (req.method === "GET") {
			res.view('user/register');
		} else {
			let params = req.allParams();

			User.create({
				uid: uuid(),
				email: params.email,
				password: params.password,
				provider: 'local',
			}).exec((err, user) => {
				if (err) {
					LogService.log(err);

					return res.view('500');
				}

				req.login(user, (err) => {
					if (err) {
						return res.send(err);
					} else {
						return res.send({
							message: info.message,
							user: user
						});
					}
				});
			});
		}
	},
};
