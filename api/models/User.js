/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require('bcrypt');

module.exports = {
	attributes: {
		uid: {
			type: 'string',
			required: true,
			unique: true,
		},
		provider: {
			type: 'string',
			required: false,
			unique: false,
		},
		email: {
			type: 'email',
			required: false,
			unique: true,
		},
		password: {
			type: 'string',
			minLength: 6,
			required: true,
		},
		toJSON: function() {
			LogService.log(this);
			if (this.toObject) {
				var obj = this.toObject();
				delete obj.password;
				LogService.log(obj);
				return obj;
			} else {
				LogService.log(this);
			}
		},
	},
	authenticate: (reqUser, cb) => {
		User.findOne({
			email: reqUser.email
		}).exec((err, user) => {
			if (err) {
				LogService.log(err);
			}
			if (!user) {
				LogService.log("User not found.");
			}

			if (err || !user) {
				cb({authenticated: false});
			}

			bcrypt.compare(reqUser.password, user.password, (err, res) => {
				if (err) LogService.log(err);

				if (!res) {
					cb({authenticated: res});

					return;
				}

				var obj = user.toObject();

				delete obj.password;
				obj.authenticated = res;

				cb(obj);
			});
		});
	},
	beforeCreate: (user, cb) => {
		// Temporarily wrap in a destroy to make sure the DB is clear.
		User.destroy({}).exec((err) => {

		bcrypt.genSalt(10, (err, salt) => {
			bcrypt.hash(user.password, salt, (err, hash) => {
				if (err) {
					LogService.log(err);
					cb(err);
				} else {
					user.password = hash;
					cb();
				}
			});
		});

		});
	},
};
