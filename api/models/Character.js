/**
 * Character.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	attributes: {
		name: {
			type: 'string',
			required: true,
			unique: true,
		},
		level: {
			type: 'integer',
			required: true,
			defaultsTo: 1,
		},
		uid: {
			type: 'string',
			required: true,
		},
		locX: {
			type: 'float',
			defaultsTo: 0,
		},
		locY: {
			type: 'float',
			defaultsTo: 0,
		},
		locZ: {
			type: 'float',
			defaultsTo: 0,
		},
		rotW: {
			type: 'float',
			defaultsTo: 0,
		},
		rotX: {
			type: 'float',
			defaultsTo: 0,
		},
		rotY: {
			type: 'float',
			defaultsTo: 0,
		},
		rotZ: {
			type: 'float',
			defaultsTo: 0,
		},
	},
};

