/**
 * Jwt.js
 *
 * @description :: https://en.wikipedia.org/wiki/JSON_Web_Token
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

let nJwt = require('njwt');
let uuid = require('uuid/v4');

module.exports = {
	attributes: {
		aud: {
			type: 'string',
			required: false,
		},
		exp: {
			type: 'integer',
			required: true,
		},
		iat: {
			type: 'integer',
			required: true,
		},
		iss: {
			type: 'string',
			required: true,
		},
		jti: {
			type: 'string',
			required: true,
		},
		sub: {
			type: 'string',
			required: true,
		},
		token: {
			type: 'string',
			required: true,
		},
		uid: {
			type: 'string',
			required: true,
			unique: true,
		},
	},
	authenticate: (req, cb) => {
		Jwt.getJwt(req, (err, verifiedJwt) => {
			LogService.log('TODO: Validate JWT was actually created and exists in DB.');

			cb(err, verifiedJwt);
		});
	},
	getJwt: (req, cb) => {
		let jwt = (req.headers && req.headers.authorization || req.params.all().JWT);

		nJwt.verify(jwt.replace(/^[^ ]+[ ]/, ""), sails.config.jwt_secret, (err, verifiedJwt) => {
			if (err) {
				cb(err, null);
			} else {
				cb(null, verifiedJwt);
			}
		});
	},
	getGameClientLogin: (user) => {
		/* This user should already be verified */
		var claims = {
			sub: user.uid,
			iss: sails.config.iss,
			permissions: 'game-client-login',
			aud: 'game-server-verify-account',
		};

		var jwt = nJwt.create(claims, sails.config.jwt_secret);
		var token = jwt.compact();
		jwt.uid = uuid();

		// Erase any old JWTs in the DB
		Jwt.destroy({
			uid: {'!': [jwt.uid]},
			sub: jwt.body.sub
		}).exec((err) => {
			if (err) LogService.log(err);
		});

		Jwt.create({
			aud: jwt.body.aud,
			exp: jwt.body.exp,
			iat: jwt.body.iat,
			iss: jwt.body.iss,
			jti: jwt.body.jti,
			sub: jwt.body.sub,
			token: token,
			uid: jwt.uid,
		}).exec((err) => {
			if (err) LogService.log(err);

			Jwt.find({sub: jwt.body.sub}).exec((err, result) => {
				if (err) LogService.log(err);
				LogService.log("================" + result.length);
			});
		});

		LogService.log(jwt);

		return token;
	},
};

