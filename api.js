let express = require('express');
let app = express();

let data = require('./data/data.js');
let loot = require('./loot/loot.js');
let lootApi = require('./loot/api.js');

let lootApiServer = new lootApi({appServer: app});

module.exports.app = app;
